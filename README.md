## format the namenode(run only in master)
```bash
$ hadoop namenode -format
```

## start hadoop
```bash 
$ start-dfs.sh
$ start-yarn.sh
```

## useful command
```bash
$ hdfs dfsadmin -report
$ hdfs dfs -ls
$ hdfs dfs -put <local file> <hdfs URI>
```