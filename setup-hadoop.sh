wget https://archive.apache.org/dist/hadoop/core/hadoop-2.7.3/hadoop-2.7.3.tar.gz
mv ./hadoop-2.7.3.tar.gz /home/hadoop/hadoop-2.7.3.tar.gz
tar zxvf /home/hadoop/hadoop-2.7.3.tar.gz -C /home/hadoop
echo -e "\nexport HADOOP_HOME=/home/hadoop/hadoop-2.7.3\nexport PATH=\$PATH:\$HADOOP_HOME/bin:\$HADOOP_HOME/sbin\nexport HADOOP_CLASSPATH=\$JAVA_HOME/lib/tools.jar" >> ~/.bashrc
source ~/.bashrc
cp ./csci4180_hadoop_conf/* /home/hadoop/hadoop-2.7.3/etc/hadoop/