wget https://bitbucket.org/davidpcaldwell/slime/downloads/jdk-8u144-linux-x64.tar.gz
sudo mkdir /usr/lib/jvm
sudo mv jdk-8u144-linux-x64.tar.gz /usr/lib/jvm/
sudo tar zxvf /usr/lib/jvm/jdk-8u144-linux-x64.tar.gz -C /usr/lib/jvm/
echo -e "export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_144 \n export PATH=\$PATH:\$JAVA_HOME/bin" >> ~/.bashrc
source ~/.bashrc
